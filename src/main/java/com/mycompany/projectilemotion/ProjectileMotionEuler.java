/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectilemotion;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author smith
 */
public class ProjectileMotionEuler {

    public static void main(String[] args) throws IOException {
        
        Scanner input = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("0.0####");
        
        // gravity
        double gravity = 0;
        // initial time
        double t = 0.0;
        // end time
        double tEnd = 0.0;
        // number of steps
        double steps = 0.0;
        // (h) the size of one step
        double h = 0;
        
        // radius of ball
        double radius = 0;
        // ball density
        double ballDensity = 0;
        // fluid density
        double fluidDensity = 0;
        // coefficient of drag
        double coeffDrag = 0;
        
        
        
        
        // position vector
        double[] P_ = {0, 0, 0};
        //System.out.println("P_ = {" + P_[0] + ", " + P_[1] + ", " + P_[2] + "}");
        // velocity ball
        double[] V_ = {0, 0, 0};
        //System.out.println("V_ = {" + V_[0] + ", " + V_[1] + ", " + V_[2] + "}");
        // velocity of wind
        double[] Vw_ = {0, 0, 0};
        //System.out.println("Vw_ = {" + Vw_[0] + ", " + Vw_[1] + ", " + Vw_[2] + "}");
        // rotation vector
        double[] R_ = {0, 0, 0};  
        //System.out.println("R_ = {" + R_[0] + ", " + R_[1] + ", " + R_[2] + "}");
        
        // Draw logo at top of screen
        drawAppLogo();
        
        // Get input for variables from user ///////////////////////////////////////////////////////
        System.out.println("Hints - enter 0 if you do not want to include something in the model.\n ");
        System.out.println("======== Projectile motion of a ball ========= ");
        System.out.println("Enter the gravity: ");
        gravity = input.nextDouble();
        System.out.println("Enter the radius: ");
        radius = input.nextDouble();
        System.out.println("Enter the drag coefficient: ");
        coeffDrag = input.nextDouble();
        System.out.println("Enter the objects density: ");
        ballDensity = input.nextDouble();
        System.out.println("Enter the fluid density: ");
        fluidDensity = input.nextDouble();
        
        // Get the Position Vector from the user //
        System.out.println("Enter the Position vector: ");
        System.out.print("Position i = ");
        P_[0] = input.nextDouble();
        System.out.print("Position j = ");
        P_[1] = input.nextDouble();
        System.out.print("Position k = ");
        P_[2] = input.nextDouble();

        // Get the Velocity Vector from the user //
        System.out.println("Enter the Velocity vector: ");
        System.out.print("Velocity i = ");
        V_[0] = input.nextDouble();
        System.out.print("Velocity j = ");
        V_[1] = input.nextDouble();
        System.out.print("Velocity k = ");
        V_[2] = input.nextDouble();
        
        // Get the Velocity of wind Vector from the user //
        System.out.println("Enter the wind vector: ");
        System.out.print("Vwind i = ");
        Vw_[0] = input.nextDouble();
        System.out.print("Vwind j = ");
        Vw_[1] = input.nextDouble();
        System.out.print("Vwind k = ");
        Vw_[2] = input.nextDouble();
        
        // Get the Rotation Vector from the user //
        System.out.println("Enter the rotation vector: ");
        System.out.print("R_ i = ");
        R_[0] = input.nextDouble();
        System.out.print("R_ j = ");
        R_[1] = input.nextDouble();
        System.out.print("R_ k = ");
        R_[2] = input.nextDouble();

        System.out.println("Enter the starting time: ");
        t = input.nextDouble();
        System.out.println("Enter the end time: ");
        tEnd = input.nextDouble();
        System.out.println("Enter h: ");
        h = input.nextDouble();

        steps = ((tEnd - t) / h);
        System.out.println("Motion will be completed in " + df.format(steps) + " steps.");

        
       
        
        // mass of ball
        double mass = ((4.0 / 3.0) * Math.PI * Math.pow(radius, 3.0)) * ballDensity;
        // frontal area
        double circleArea = Math.PI * Math.pow(radius, 2.0); 

        // length of rotation vector
        double omega = Math.sqrt(R_[0] * R_[0] + R_[1] * R_[1] + R_[2] * R_[2]);
        //System.out.println("Omega = " + omega);
        
        
        
               
////////////////////////////////////////////////////// FIND FORCE OF GRAVITY ////////////////////////////////////////////////////////////////////////
        // Force of Gravity just in k
        double Fk = -1.0 * mass * gravity;
        
        // Force of Gravity (Neutons)
        double[] Fg_ = {0, 0, Fk};
        //System.out.println("Fg = {" + Fg_[0] + ", " + Fg_[1] + ", " + Fg_[2] + "}");
        
        
        
//////////////////////////////////////////////////FIND APPARENT VELOCITY AND IT'S LENGTH ////////////////////////////////////////////////////////////      
        // apparent velocity
        double[] Va_ = {V_[0] - Vw_[0], V_[1] - Vw_[1], V_[2] - Vw_[2]};
        //System.out.println("Va_ = {" + Va_[0] + ", " + Va_[1] + ", " + Va_[2] + "}");
        
        // length of apparent velocity
        double Va = Math.sqrt(Va_[0] * Va_[0] + Va_[1] * Va_[1] + Va_[2] * Va_[2]);
        //System.out.println("Fg = {" + Va + "}");
       
               
        
////////////////////////////////////////////////////////////FIND FORCE OF DRAG //////////////////////////////////////////////////////////////////////      
        double d = 0.5 * fluidDensity * circleArea;
        double Fd = d * coeffDrag * Math.pow(Va, 2);
        double[] FdHat = {Va_[0]/-Va, Va_[1]/-Va, Va_[2]/-Va};
        double[] Fd_ = {Fd * FdHat[0], Fd * FdHat[1], Fd * FdHat[2]};
        //System.out.println("d = {" + d + "}");
//        System.out.println("Fd = {" + Fd + "}");
//        System.out.println("FdHat = {" + FdHat[0] + ", " + FdHat[1] + ", " + FdHat[2] + "}");      
//        System.out.println("Fd_ = {" + Fd_[0] + ", " + Fd_[1] + ", " + Fd_[2] + "}");
        
        
               
////////////////////////////////////////////////////////////FIND FORCE OF MAGNUS ////////////////////////////////////////////////////////////////////
        double Fm = d * radius * omega * Va;
        //System.out.println("Fm = {" + Fm + "}");

        // cross product of R and Va 
        double[] R_XVa_ = {
            (R_[1] * Va_[2] - Va_[1] * R_[2]),
            -(R_[0] * Va_[2] - Va_[0] * R_[2]),
            (R_[0] * Va_[1] - Va_[0] * R_[1])};
        
        //System.out.println("R_XVa_ = {" + R_XVa_[0] + ", " + R_XVa_[1] + ", " + R_XVa_[2] + "}");
  
        // length of R_ X Va_
        double lengthOfR_XVa_ = Math.sqrt(R_XVa_[0] * R_XVa_[0] + R_XVa_[1] 
                * R_XVa_[1] + R_XVa_[2] * R_XVa_[2]);
        
        //System.out.println("lengthOfR_XVa_ = {" + lengthOfR_XVa_ + "}");
        
        double[] FmHat = {R_XVa_[0] / lengthOfR_XVa_, R_XVa_[1] / lengthOfR_XVa_, R_XVa_[2] / lengthOfR_XVa_};
        //System.out.println("FmHat = {" + FmHat[0] + ", " + FmHat[1] + ", " + FmHat[2] + "}");
        
        double[] Fm_ = {Fm * FmHat[0], Fm * FmHat[1], Fm * FmHat[2]};
        //System.out.println("Fm_ = {" + Fm_[0] + ", " + Fm_[1] + ", " + Fm_[2] + "}");
        
              
        
////////////////////////////////////////////////////////////FIND NET FORCE //////////////////////////////////////////////////////////////////////////
        double[] Fnet_ = {Fg_[0] + Fd_[0] + Fm_[0], Fg_[1] + Fd_[1] + Fm_[1], Fg_[2] + Fd_[2] + Fm_[2]};
        //System.out.println("Fnet_ = {" + Fnet_[0] + ", " + Fnet_[1] + ", " + Fnet_[2] + "}");

        
        
////////////////////////////////////////////////////////////FIND ACCELERATION ///////////////////////////////////////////////////////////////////////
        double[] a_ = {1.0/mass * Fnet_[0], 1.0/mass * Fnet_[1], 1.0/mass * Fnet_[2]};
        //System.out.println("a_ = {" + a_[0] + ", " + a_[1] + ", " + a_[2] + "}");
        
        
        // output text file
        FileWriter output = new FileWriter("output.txt");

        System.out.println("\nn = 0" + ":");
        System.out.println("t = " + df.format(t));
        System.out.println("P_ = {" + P_[0] + ", " + P_[1] + ", " + P_[2] + "}");
        System.out.println("V_ = {" + V_[0] + ", " + V_[1] + ", " + V_[2] + "}");
        System.out.println("a_ = {" + a_[0] + ", " + a_[1] + ", " + a_[2] + "}");


        //Euler
        int stepCount = 0;
        while (stepCount < steps) {
            //add step count
            stepCount++;

            System.out.println("\nn = " + stepCount + ":");
            //output.write("\nStep " + stepCount + ":\n");
            
            
            
            // t
            t = t + h;
            
            
            // position
            P_[0] = P_[0] + h * V_[0];
            P_[1] = P_[1] + h * V_[1];
            P_[2] = P_[2] + h * V_[2];

            // velocity
            V_[0] = V_[0] + h * a_[0];
            V_[1] = V_[1] + h * a_[1];
            V_[2] = V_[2] + h * a_[2];

            // apparent velocity
            Va_[0] = V_[0] - Vw_[0];
            Va_[1] = V_[1] - Vw_[1];
            Va_[2] = V_[2] - Vw_[2];
            //System.out.println("Va_ = {" + Va_[0] + ", " + Va_[1] + ", " + Va_[2] + "}");
            
            // length of apparent velocity
            Va = Math.sqrt(Va_[0] * Va_[0] + Va_[1] * Va_[1] + Va_[2] * Va_[2]);
            //System.out.println("Va = {" + Va + "}");
            
            // Force Drag
            Fd = d * coeffDrag * Math.pow(Va, 2.0);
            //System.out.println("Fd = {" + Fd + "}");
             
            FdHat[0] = Va_[0]/-Va;
            FdHat[1] = Va_[1]/-Va;
            FdHat[2] = Va_[2]/-Va;
            //System.out.println("FdHat = {" + FdHat[0] + ", " + FdHat[1] + ", " + FdHat[2] + "}");

            
            Fd_[0] = Fd * FdHat[0]; 
            Fd_[1] = Fd * FdHat[1]; 
            Fd_[2] = Fd * FdHat[2];
            //System.out.println("Fd_ = {" + Fd_[0] + ", " + Fd_[1] + ", " + Fd_[2] + "}");
            
            // Force Magnus
            Fm = d * radius * omega * Va;
            //System.out.println("Fm = {" + Fm + "}");

            // cross product of R and Va 
            R_XVa_[0] = R_[1] * Va_[2] - Va_[1] * R_[2];
            R_XVa_[1] = -(R_[0] * Va_[2] - Va_[0] * R_[2]);
            R_XVa_[2] = R_[0] * Va_[1] - Va_[0] * R_[1];
            //System.out.println("R_XVa_ = {" + R_XVa_[0] + ", " + R_XVa_[1] + ", " + R_XVa_[2] + "}");
  
            // length of R_ X Va_
            lengthOfR_XVa_ = Math.sqrt(R_XVa_[0] * R_XVa_[0] + R_XVa_[1] * R_XVa_[1] + R_XVa_[2] * R_XVa_[2]);
            //System.out.println("lengthOfR_XVa_ = {" + lengthOfR_XVa_ + "}");
            
            FmHat[0] = R_XVa_[0]/lengthOfR_XVa_;
            FmHat[1] = R_XVa_[1]/lengthOfR_XVa_;
            FmHat[2] = R_XVa_[2]/lengthOfR_XVa_;
            //System.out.println("FmHat = {" + FmHat[0] + ", " + FmHat[1] + ", " + FmHat[2] + "}");
        
            Fm_[0] = Fm * FmHat[0]; 
            Fm_[1] = Fm * FmHat[1];
            Fm_[2] = Fm * FmHat[2];
            //System.out.println("Fm_ = {" + Fm_[0] + ", " + Fm_[1] + ", " + Fm_[2] + "}");
            
            // Net Force
            Fnet_[0] = Fg_[0] + Fd_[0] + Fm_[0]; 
            Fnet_[1] = Fg_[1] + Fd_[1] + Fm_[1];
            Fnet_[2] = Fg_[2] + Fd_[2] + Fm_[2];
            //System.out.println("Fnet_ = {" + Fnet_[0] + ", " + Fnet_[1] + ", " + Fnet_[2] + "}");
            
            // Acceleration
            a_[0] = 1.0/mass * Fnet_[0];
            a_[1] = 1.0/mass * Fnet_[1];
            a_[2] = 1.0/mass * Fnet_[2];
            //System.out.println("a_ = {" + a_[0] + ", " + a_[1] + ", " + a_[2] + "}");

            
            
            
            System.out.println("t = " + df.format(t));
            System.out.println("P_ = {" + P_[0] + ", " + P_[1] + ", " + P_[2] + "}");
            System.out.println("V_ = {" + V_[0] + ", " + V_[1] + ", " + V_[2] + "}");
            System.out.println("a_ = {" + a_[0] + ", " + a_[1] + ", " + a_[2] + "}");
            
        }

        

        

    }

    public static void drawAppLogo() {
        System.out.println("========================================================================================");
        System.out.println("========================================================================================");
        System.out.println("===        #     #                                                    #     #        ===");
        System.out.println("===      ##       ##                                                ##       ##      ===");
        System.out.println("===    ###   # #   ###                                            ###   # #   ###    ===");
        System.out.println("===   ###    ###    ###                                          ###    ###    ###   ===");
        System.out.println("===  #####  #####  #####                Euler                   #####  #####  #####  ===");
        System.out.println("=== #####################         Motion of projectile         ##################### ===");
        System.out.println("=== #####################                                      ##################### ===");
        System.out.println("===  ###################                                        ###################  ===");
        System.out.println("===   #### # ### # ####                                          #### # ### # ####   ===");
        System.out.println("===    ###    #    ###                                            ###    #    ###    ===");
        System.out.println("===      ##       ##                                                ##       ##      ===");
        System.out.println("===        #     #                                                    #     #        ===");
        System.out.println("========================================================================================");
        System.out.println("========================================================================================");
    }
    
     // Scanner to take in user inputs //////////////////////////////////////////////////////////
        
        

        // Draw batman ascii logo //////////////////////////////////////////////////////////////////
//        drawAppLogo();
//        
//        System.out.println("\n*** Results ***");
//            System.out.println("Step " + count);
//
//            System.out.println("t" + count + " = " + t);
//            System.out.println("p" + count + " = {" + df.format(p_[0]) + ", " + df.format(p_[1]) + ", "
//                    + df.format(p_[2]) + "}");
//            System.out.println("v" + count + " = {" + df.format(v_[0]) + ", " + df.format(v_[1])
//                    + ", " + df.format(v_[2]) + "}");
           

        

        //print out initial conditions
        //System.out.print("Initial conditions:\n\n"
//                + "tZero (initial time) = " + t + "\n"
//                + "tEnd (end time) = " + tEnd + "\n"
//                + "Steps = " + steps + "\n"
//                + "Delta Time (h) = " + deltaTime + "\n"
//                + "Position Vector = " + Arrays.toString(pBar) + "\n"
//                + "Velocity Vector = " + count + " = {" + df.format(vBar[0]) + ", " + df.format(vBar[1]) 
//                + ", " + df.format(vBar[2]) + "}"
                //+ "Velocity Wind Vector = " + Arrays.toString(Vw_) + "\n"
                //+ "Apparent Velocity Vector = " + Arrays.toString(vAppBar) + "\n"
                //+ "Length of Apparent Velocity = " + df.format(vAppL) + "\n"
//                + "Rotation Vector = " + Arrays.toString(pseudoVec) + "\n"
//                + "Omega = " + df.format(omega) + "\n"
//                + "Radius of Ball (r) = " + radius + "\n"
//                + "Density of Ball = " + ballDensity + "\n"
//                + "Mass of Ball (m) = " + df.format(mass) + "\n"
//                + "Frontal Area (A) = " + df.format(AreaOfCircle) + "\n"
//                + "Gravitational Acceleration (g) = " + GRAVITY + "\n"
//                + "Density of Fluid = " + fluidDensity + "\n"
//                + "Coefficient of Drag (CD) = " + coeffDrag + "\n"
                //+ "Viscocity of Fluid = " + visc + "\n"
                //+ "Magnus Coefficient (CL) = " + df.format(coeffMagnus) + "\n"
               // + "Cross product of R and Va = " + Arrays.toString(crossPseudoVecVAppBar) + "\n"
                //+ "Length of cross R and Va = " + df.format(crossOmegaVAppL) + "\n"
                //+ "\nP 0 = " + Arrays.toString(pBar) + "\n"
                //+ "\nVa 0 = " + Arrays.toString(vAppBar) + "\n"
        //);

        //output.write("Initial conditions:\n\n"
//                + "tZero (initial time) = " + t + "\n"
//                + "tEnd (end time) = " + tEnd + "\n"
//                + "Steps = " + steps + "\n"
//                + "Delta Time (h) = " + deltaTime + "\n"
//                + "Position Vector = " + Arrays.toString(pBar) + "\n"
//                + "Velocity Vector = " + Arrays.toString(vBar) + "\n"
//                + "Velocity Wind Vector = " + Arrays.toString(Vw_) + "\n"
                //+ "Apparent Velocity Vector = " + Arrays.toString(vAppBar) + "\n"
                //+ "Length of Apparent Velocity = " + vAppL + "\n"
                //+ "Rotation Vector = " + Arrays.toString(pseudoVec) + "\n"
                //+ "Omega = " + omega + "\n"
//                + "Radius of Ball (r) = " + radius + "\n"
//                + "Density of Ball = " + ballDensity + "\n"
//                + "Mass of Ball (m) = " + mass + "\n"
//                + "Frontal Area (A) = " + AreaOfCircle + "\n"
//                + "Gravitational Acceleration (g) = " + GRAVITY + "\n"
//                + "Density of Fluid = " + fluidDensity + "\n"
//                + "Coefficient of Drag (CD) = " + coeffDrag + "\n"
                //+ "Viscocity of Fluid = " + visc + "\n"
                //+ "Magnus Coefficient (CL) = " + coeffMagnus + "\n"
               // + "Cross product of R and Va = " + Arrays.toString(crossPseudoVecVAppBar) + "\n"
                //+ "Length of cross R and Va = " + crossOmegaVAppL + "\n"
                //+ "\nP 0 = " + Arrays.toString(pBar) + "\n"
                //+ "\nVa 0 = " + Arrays.toString(vAppBar) + "\n"
        //);

}

